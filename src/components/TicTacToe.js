import React, { Component } from 'react'


export class TicTacToe extends Component {
  render() {
    return (
        <section onClick={this.props.toggle}  className="boards">
        <div className='row one'>
        <span className='block 0' > </span>
        <span className='block 1 borderRightLeft'></span>
        <span className='block 2'></span>
        </div>
    
        <div className='row two'>
        <span className='block 3'></span>
        <span className='block 4 borderRightLeft'></span>
        <span className='block 5'></span>
        </div>
    
        <div className="row three">
        <span className='block 6'></span>
        <span className='block 7 borderRightLeft'></span>
        <span className='block 8'></span>
         </div>
        </section>
    )
  }
}

export default TicTacToe