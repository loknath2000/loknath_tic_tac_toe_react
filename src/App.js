import React,{Component} from 'react'
import TicTacToe from './components/TicTacToe';

export default class App extends Component{
    constructor(props){
        super(props);
        this.state={
            history:[],
            playerOne: [],
            playerTwo: [],
            nextTurnX: true,
            message: "Player's Turn: X",
            checkingWinner : this.checkWinner
        };
    }
    
    
    //arrow function to bind this with it
    toggleFunction =(event)=> {

        // checking and storing moves inside array
        if(!this.state.nextTurnX && event.target.innerText === "" && !this.state.message.includes("Winner")){
            
            //storing the number of block clicked in respective players array
            this.setState({
                ...this.state,
                playerTwo:[...this.state.playerTwo,event.target.classList[1]],
                message: this.state.nextTurnX?"Player's Turn: O":"Player's Turn: X",
                history:[...this.state.history,event.target.classList[1]],
                nextTurnX: !this.state.nextTurnX
            })
            
            //showing O on user interface
            event.target.innerText= "O";

        
        }else if(this.state.nextTurnX && event.target.innerText === "" && !this.state.message.includes("Winner")){
            
            this.setState({
                ...this.state,
                playerOne:[...this.state.playerOne,event.target.classList[1]],
                message: this.state.nextTurnX?"Player's Turn: O":"Player's Turn: X",
                history:[...this.state.history,event.target.classList[1]],
                nextTurnX: !this.state.nextTurnX
            })
            
            event.target.innerText = "X";

        }

        let result = this.state.checkingWinner;

        //to update states we are giving some time so winner will be checked after updation of states completed
        setTimeout(()=>{result()},0);
    }

    checkWinner = ()=> {
       
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
          ]

        for(let i = 0; i < lines.length; i++ ) {
            
            //destructuring winning lines to numbers
            const [a,b,c] = lines[i];
           
            const one = this.state.playerOne.includes(String(a));
            const two = this.state.playerOne.includes(String(b));
            const three = this.state.playerOne.includes(String(c));
            const four = this.state.playerTwo.includes(String(a));
            const five = this.state.playerTwo.includes(String(b));
            const six = this.state.playerTwo.includes(String(c));
            
            if(one === true && two == true && three == true){

                this.setState({
                    ...this.state,
                    message: "Winner X"
                })
              
            }else if(four==true && five == true && six == true){
                
                this.setState({
                    ...this.state,
                    message: "Winner O"
                })
              
            } else if(this.state.playerOne.length+ this.state.playerTwo.length >= 9){
                
                this.setState({
                    ...this.state,
                    message: "Tie"
                })
                
            }
           
        }  
    
    }

    
    jumpTo = (index) =>{
        
        if(index == 0 ){

            for(let i = 0; i < 9; i ++){
                
                document.getElementsByClassName(String(i))[0].innerText = "";
            
            }
            
            this.setState({
                ...this.state,
                history:[],
                playerOne: [],
                playerTwo: [],
                nextTurnX: true,
                message: "Player's Turn: X"
            })

        }else{
          
        for(let i = 0; i < 9; i ++){
            
            document.getElementsByClassName(String(i))[0].innerText = "";
        
        }
       
        if(this.state.history.length > 0){

            let historyOne =[];
            let historyTwo =[];
            let updatedHistory = [];

            for(let i = 0; i < index; i++){
                
                updatedHistory.push(this.state.history[i]);
                
                if(i% 2 === 0){
                   
                    historyOne.push(this.state.history[i])
                
                }else{
                
                    historyTwo.push(this.state.history[i])
                
                }
            }

            historyOne.forEach((value)=>{
            
                document.getElementsByClassName(String(value))[0].innerText = "X";
            
            })
            
            historyTwo.forEach((value)=>{
            
                document.getElementsByClassName(String(value))[0].innerText = "O";
            
            })
           
            if(index%2 ==0){

                this.setState({
                    ...this.state,
                    nextTurnX:true,
                    playerOne: historyOne,
                    playerTwo: historyTwo,
                    history: updatedHistory,
                    message: "Player's Turn: X"
                })

            }else{

                this.setState({
                    ...this.state,
                    nextTurnX:false,
                    playerOne: historyOne,
                    playerTwo: historyTwo,
                    history: updatedHistory,
                    message: "Player's Turn: O"
                })

            }
            

        }
    }
}


    render(){
        const history = this.state.history;

        let moves = this.state.history.map((value,index)=>{
            const description = index ? "Go To Move #"+ index: "Go To Game Start" 
            return(
                <li key={index}>
                    <button onClick={()=>{this.jumpTo(index)}}>{description}</button>
                </li>
            )
        })
        
        return(
        
        <section className ="outerContainer">
            <div className='mainContainer'>
            <h1 className="heading">Tic-Tac-Toe</h1>
            
            <TicTacToe toggle= {this.toggleFunction}/>
           
            </div>
             <ul>
             <h2 className = "message">{this.state.message} </h2>
                 {moves}</ul>
        </section>
        )
    }
}
